# Prova prática DevOps

O objetivo do teste é a construção de um `docker-compose` que permita inicializar alguns containers, 
conectá-los e configurar o basico de um projeto php com o framework Laravel. 

1. O projeto deve conter os seguintes arquivos e pastas.

    ```
   apache2
   laravel
   php-fpm
   .env
   .env.example
   .gitignore
   docker-compose.yaml
   README.md
    ```

2. Na pasta laravel, deve ser baixado o projeto do seguinte repositorio: https://github.com/laravel/laravel
3. Inicialize um repositorio git 
4. Configure o `docker-compose.yaml`.
    - O arquivo deve possuir 4 serviços `apache2`, `php-fpm`, `postgres` e `workspace`.
    
    - O serviço `apache2`: 
        - utilizar o `Dockerfile` existe na pasta `apache2`. 
        - volume: a pasta `laravel` deve ser visivel dentro do container em `/var/www/`
        - volume: a pasta `apache2/sites` deve ser visivel dentro do container em `/etc/apache2/sites-available`
    
    - O serviço `php-fpm`: 
        - image: letsdockerize/laradock-php-fpm:2.4-7.3
        - volume: a pasta laravel deve ser visivel dentro do container em `/var/www/` 
        - volume: `./php-fpm/php7.3.ini:/usr/local/etc/php/php.ini`
        - exponha a porta 9000
        
    - O serviço `postgres`:
        - image: postgres:11.6-alpine
        
    - O serviço `workspace`:
        - image: letsdockerize/laradock-workspace:2.4-7.3
        - defina o workdir para `/var/www`
        - volume: a pasta laravel deve ser visivel dentro do container em `/var/www/`
         
    - crie uma rede de nome `backend` do tipo `driver(bridge)` e adicione a todos os serviços  

5. Configure o arquivo `.env` com as seguintes variaveis e ajuste o `docker-compose.yaml`.
    ```
    APACHE2_PORT=8085
    
    POSTGRES_PORT=5432
    POSTGRES_DATABASE=default
    POSTGRES_USER=default
    POSTGRES_PASSWORD=secret
    ```
   
6. Crie os containers
7. Entre no container `workspace` e execute os comandos dentro da pasta laravel
    -  `composer install --prefer-dist --no-dev --no-interaction`.
    -  `php artisan key:generate`.
    - altere o `.env` da aplicação.
        ```
            DB_CONNECTION=pgsql
            DB_HOST=postgres
            DB_PORT=5432
            DB_DATABASE=default
            DB_USERNAME=default
            DB_PASSWORD=secret
        ```
    -  `php artisan migrate`.
8. Acesse a aplicação no endereço `localhost:8085`.
9. publique o projeto para o `github` e nos envie o caminho do repositorio.